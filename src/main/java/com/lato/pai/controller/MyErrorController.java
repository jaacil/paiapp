package com.lato.pai.controller;

import org.junit.platform.commons.logging.Logger;
import org.junit.platform.commons.logging.LoggerFactory;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@Controller
public class MyErrorController implements ErrorController {

    static final Logger log =
            LoggerFactory.getLogger(MainPageController.class);

    @RequestMapping("/error")
    public String handleError(HttpServletRequest request) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        if (status != null) {
            Integer statusCode = Integer.valueOf(status.toString());

            if(statusCode == HttpStatus.NOT_FOUND.value()) {
                log.error(() -> ("Page not found: " + HttpStatus.NOT_FOUND.toString()));
                return "error-404";
            }
            else if(statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
                log.error(() -> ("Internal server error: " + HttpStatus.INTERNAL_SERVER_ERROR.toString()));
                return "error-500";
            }
        }
        return "error";
    }

//    @Override
//    public String getErrorPath() {
//        return null;
//    }
}
