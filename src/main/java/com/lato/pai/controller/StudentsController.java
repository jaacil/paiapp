package com.lato.pai.controller;

import com.lato.pai.model.*;
import com.lato.pai.repository.*;
import com.lato.pai.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Controller
public class StudentsController {

    @Autowired
    UserService userService;

    @Autowired
    SubjectService subjectService;

    @Autowired
    ClassListService classListService;

    @Autowired
    UserClassService userClassService;

    @Autowired
    GradeService gradeService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserClassRepository userClassRepository;

    @Autowired
    GradeRepository gradeRepository;

    @Autowired
    AdminRepository adminRepository;

    @Autowired
    SubjectRepository subjectRepository;

    private static int userClassID;
    private static int userToEditID;
    private static HashMap<String, List<Grade>> map2 = new HashMap<>();

    @GetMapping("/studentsList")
    public String registerAdminPage(Model model) {
        List<User> studentsList = userService.findAllStudents();
        List<ClassList> classLists = classListService.findAllClasses();
        model.addAttribute("studentsList", studentsList);
        model.addAttribute("classesList", classLists);
        return "students-list";
    }

    @ModelAttribute("klasa")
    public void modelClass(Model model) {

        List<UserClass> studentList = userClassService.findAllByClassID(userClassID); // znajdz liste studentow uczeszczajacych do klasy o id

        model.addAttribute("studentsOfClassList", studentList);
        model.addAttribute("grade", new Grade());

        model.addAttribute("mapOfGrade", map2);
    }

    @ModelAttribute("studentToEdit")
    public void modelStudentEditAdmin(Model model) {
        User student = userRepository.findById(userToEditID);
        List<ClassList> classLists = classListService.findAllClasses();

        model.addAttribute("classesList", classLists);
        model.addAttribute("studentToEdit", student);
    }

    @ModelAttribute("studentT")
    public void modelStudentEditStudent (Model model, Principal principal){
        User student = userRepository.findByLogin(principal.getName());
        model.addAttribute("studentT", student);
    }

    @PostMapping("/menageClass")
    public String selectClass(ClassList classesList, Model model) {

        List<UserClass> userClass = userClassService.userClass(classesList.getName()); //znajdz wszystkie rekordy po nazwie klasy

        if(userClass.isEmpty()) {
            return "error";
        }

        UserClass result = userClass.get(0); //wybierz pierwszy
        userClassID = result.getClassList().getId(); //wybierz id klasy (foreign key) z rekordu

        List<UserClass> studentList = userClassService.findAllByClassID(userClassID); // znajdz liste studentow uczeszczajacych do klasy o id

        if(studentList.isEmpty()) {
            return "error";
        }

        model.addAttribute("studentsOfClassList", studentList);
        model.addAttribute("grade", new Grade());

        makeAGradesHashMap();

        model.addAttribute("mapOfGrade", map2);

        return "menage-one-class";
    }

    @GetMapping("/edit")
    public String editPage(Model model, Principal principal) {
        User student = userRepository.findByLogin(principal.getName());
        model.addAttribute("studentT", student);
        return "edit-student";
    }

    @GetMapping("/studentEditAdminPage/{id}")
    public String editStudentPage(Model model, @PathVariable("id") int id) {
        User student = userRepository.findById(id);
        List<ClassList> classLists = classListService.findAllClasses();
        userToEditID = id;
        model.addAttribute("classesList", classLists);
        model.addAttribute("studentToEdit", student);
        return "edit-student-admin";
    }

    @PostMapping("/editUserAdmin")
    public String processStudentEditByAdmin(@Valid @ModelAttribute("studentToEdit") User studentToEdit, BindingResult bindingResult) {

        if(bindingResult.hasErrors()) {
//            System.out.println("Errors + " + bindingResult.getAllErrors() + "\n");
            return "edit-student-admin";
        }

        userService.updateStudentByAdmin(studentToEdit);
        return "redirect:/studentsList";
    }

    @PostMapping("/editStudent")
    public String processEdit(@Valid @ModelAttribute("studentT") User studentT, BindingResult bindingResult, Principal principal) {

        if(bindingResult.hasErrors()) {
            System.out.println("Errors + " + bindingResult.getAllErrors() + "\n");
            return "edit-student";
        }

        userService.updateStudent(studentT, principal);
        return "redirect:/menu";
    }

    @GetMapping("/delete/{id}")
    public String deleteStudent(@PathVariable("id") int id) {
        User student = userRepository.findById(id);
        UserClass userClass = userClassRepository.findByUserId(id);
        List<Grade> gradesList = gradeService.findUserGrades(id);
        System.out.println(gradesList);

        if(!gradesList.isEmpty()) {
            gradeRepository.deleteAll(gradesList);
        }

        userClassService.deleteUserClass(userClass);
        userRepository.delete(student);

        return "redirect:/studentsList";
    }

    @GetMapping("/studentGrades")
    public String studentGradesPage(Principal principal, Model model) {
        User student = userRepository.findByLogin(principal.getName());
        List<Grade> gradeList = gradeService.findAllUserGrades(student);
        List<Subject> subjectList = subjectService.findAllSubjects();
        model.addAttribute("subjectsList", subjectList);
        model.addAttribute("gradesList", gradeList);
        return "grades-list";
    }

    @PostMapping("/addStudentGrade")
    public String processAddingGrade(@ModelAttribute("klasa") Grade grade, Principal principal, @RequestParam("studentId") int studentId) {
        Admin teacher = adminRepository.findByLogin(principal.getName());
        Subject subject = subjectRepository.findByAdmin(teacher);
        User student = userRepository.findById(studentId);

        grade.setGrade(Double.parseDouble(String.valueOf(grade.getGrade())));
        grade.setDesc(grade.getDesc());
        grade.setAdmin(teacher);
        grade.setSubject(subject);
        grade.setUser(student);

        gradeRepository.save(grade);

        makeAGradesHashMap();

        return "menage-one-class";
    }

    public void makeAGradesHashMap() {
        List<UserClass> studentList = userClassService.findAllByClassID(userClassID); // znajdz liste studentow uczeszczajacych do klasy o id

        User student = null;
        List<Grade> gradeList = new ArrayList<>();

        for(int i = 0; i < studentList.size(); i++) {
            student = studentList.get(i).getUser();
            gradeList = gradeService.findAllUserGrades(student);
            map2.put(student.getName(), gradeList);
        }
    }


}
