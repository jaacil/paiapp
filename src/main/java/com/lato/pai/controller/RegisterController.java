package com.lato.pai.controller;

import com.lato.pai.model.Admin;
import com.lato.pai.model.ClassList;
import com.lato.pai.model.ConfirmationToken;
import com.lato.pai.model.User;
import com.lato.pai.service.AdminService;
import com.lato.pai.service.ClassListService;
import com.lato.pai.service.ConfirmationTokenService;
import com.lato.pai.service.UserService;
import org.dom4j.rule.Mode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Controller
public class RegisterController {

    @Autowired
    UserService userService;

    @Autowired
    AdminService adminService;

    @Autowired
    ConfirmationTokenService confirmationTokenService;

    @Autowired
    ClassListService classListService;

    @ModelAttribute("user")
    public void modelStudent(Model model) {
        List<ClassList> classLists = classListService.findAllClasses();
        model.addAttribute("user", new User());
        model.addAttribute("classesList", classLists);
    }

    @ModelAttribute("admin")
    public void modelTeacher(Model model) {
        model.addAttribute("admin", new Admin());
    }


    @PostMapping("/userRegister")
    public String processUserRegister(@Valid @ModelAttribute("user") User user, BindingResult bindingResult) {

        if(bindingResult.hasErrors()) {
            return "register";
        }

        userService.signUpUser(user);
        return "index";
    }

    @PostMapping("/adminRegister")
    public String processAdminRegister(@Valid @ModelAttribute("admin") Admin admin, BindingResult bindingResult) {

        if(bindingResult.hasErrors()) {
            return "add-admin";
        }

        userService.signUpAdmin(admin);
        return "index";
    }

    @GetMapping("/confirm")
    String confirmMail(@RequestParam("token") String token) {

        Optional<ConfirmationToken> optionalConfirmationToken = confirmationTokenService.findConfirmationTokenByToken(token);

        optionalConfirmationToken.ifPresent(userService::confirmUser);

        return "/login";
    }

}
