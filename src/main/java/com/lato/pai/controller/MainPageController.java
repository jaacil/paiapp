package com.lato.pai.controller;

import com.lato.pai.model.Admin;
import com.lato.pai.model.ClassList;
import com.lato.pai.model.User;
import com.lato.pai.service.ClassListService;
import com.lato.pai.service.UserService;
import org.dom4j.rule.Mode;

import org.junit.platform.commons.logging.Logger;
import org.junit.platform.commons.logging.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;


@Controller
public class MainPageController {

    @Autowired
    ClassListService classListService;

    @Autowired
    UserService userService;

    @GetMapping("/")
    public String mainPage() {
        return "index";
    }

    static final Logger log =
            LoggerFactory.getLogger(MainPageController.class);

    @GetMapping("/register")
    public String registerPage(Model model) {
        List<ClassList> classLists = classListService.findAllClasses();
        model.addAttribute("user", new User());
        model.addAttribute("classesList", classLists);
        return "register";
    }


    @GetMapping("/adminRegisterPage")
    public String registerAdminPage(Model model) {
        model.addAttribute("admin", new Admin());
        return "add-admin";
    }

    @GetMapping("/login")
    public String loginPage() {
        return "login";
    }

    @GetMapping("/menu")
    public String contactPage(Model model) {

        List<ClassList> classLists = classListService.findAllClasses();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        boolean isAdmin = auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ADMIN"));;

        model.addAttribute("role", isAdmin);
        model.addAttribute("classesList", classLists);
        model.addAttribute("classNo", new ClassList());

        return "login-success";
    }

    @GetMapping("/accessDenied")
    public String getAccessDenied() {
        return "access-denied";
    }

    @GetMapping("/changePasswdPage")
    public String changePasswdPage() {
        return "change-passwd";
    }

    @PostMapping("/changePassword")
    public String changePasswd (Principal principal, @RequestParam("oldPassword") String oldPassword, @RequestParam String newPassword,
                                RedirectAttributes redirectAttributes) {

        try {
            userService.changePassword(principal, oldPassword, newPassword);
        } catch (Exception e) {
            log.error(() -> ("The old password " + oldPassword + " don't match to password in DB!"));
            redirectAttributes.addFlashAttribute("error", "Old password dont match!");
            return "redirect:/changePasswdPage";
        }

        return "redirect:/menu";
    }
}
