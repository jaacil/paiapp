package com.lato.pai;

import com.lato.pai.model.User;
import com.lato.pai.model.UserRole;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


//@ComponentScan({"com.lato.pai.controller"})
//@EnableJpaRepositories({"com.lato.pai.repository"})

@SpringBootApplication
public class PaiappApplication {

    public static void main(String[] args) {
        SpringApplication.run(PaiappApplication.class, args);
    }

//    @PostConstruct
//    public void init() {
//        new User(1 , "jlato", "Jacek", "User", "jacek@email.com", "password", "123456789", "Lubartów", "Ks. J. Popiełuszki", "4A", UserRole.USER, false, true);
//    }

}
