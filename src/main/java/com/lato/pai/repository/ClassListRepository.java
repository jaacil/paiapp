package com.lato.pai.repository;

import com.lato.pai.model.ClassList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClassListRepository extends JpaRepository<ClassList, Integer> {

    ClassList findByName(String name);

}
