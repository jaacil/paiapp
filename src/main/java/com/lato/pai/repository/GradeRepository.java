package com.lato.pai.repository;

import com.lato.pai.model.Admin;
import com.lato.pai.model.Grade;
import com.lato.pai.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GradeRepository extends JpaRepository<Grade, Integer> {

    public List<Grade> findAllByUserOrderBySubjectDesc(User student);
    public List<Grade> findAllByUserId(int id);
}
