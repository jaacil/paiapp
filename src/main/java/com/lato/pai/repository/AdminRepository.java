package com.lato.pai.repository;

import com.lato.pai.model.Admin;
import com.lato.pai.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminRepository extends JpaRepository<Admin, Integer> {

    Admin findByEmail(String email);
    Admin findByLogin(String login);
}
