package com.lato.pai.repository;

import com.lato.pai.model.ConfirmationToken;
import com.lato.pai.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ConfirmationTokenRepository extends JpaRepository<ConfirmationToken, Integer> {

    Optional<ConfirmationToken> findConfirmationTokenByConfirmationToken(String token);

}
