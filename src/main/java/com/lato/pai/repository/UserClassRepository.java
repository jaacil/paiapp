package com.lato.pai.repository;

import com.lato.pai.model.UserClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserClassRepository extends JpaRepository<UserClass, Integer> {

    List<UserClass> findByClassList_Name(String classListName);
    List<UserClass> findAllByClassList_Id(int id);
    UserClass findByUserId(int id);
}
