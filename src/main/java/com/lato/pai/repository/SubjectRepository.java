package com.lato.pai.repository;

import com.lato.pai.model.Admin;
import com.lato.pai.model.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, Integer> {

    public Subject findByAdmin(Admin teacher);

}
