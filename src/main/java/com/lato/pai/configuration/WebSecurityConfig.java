package com.lato.pai.configuration;

import com.lato.pai.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.sql.DataSource;

@Configuration
@AllArgsConstructor
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserService userService;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
            http.authorizeRequests()
                    .antMatchers("/", "/css/**", "/js/**", "/images/**", "/h2-console/**", "/confirm/**", "/webjars/**").permitAll()
//                    .antMatchers("/", "/css/**", "/js/**", "/images/**", "/h2-console/**", "/confirm/**", "/adminRegister/**", "/adminRegisterPage/**", "/register/**", "/userRegister/**", "/editStudent/**", "/edit/**").permitAll() //dla dodawwania przy starcie
                    .antMatchers("/adminRegister/**", "/adminRegisterPage/**", "/studentEditAdminPage/**", "/register/**", "/userRegister/**", "/menageClass/**", "/delete/**", "/editUserAdmin/**" , "/addStudentGrade/**").hasAuthority("ADMIN")
                    .antMatchers("/editStudent/**", "/edit/**", "/studentGrades/**").hasAuthority("USER")
                    .antMatchers("/changePassword/**", "/changePasswdPage/**").hasAnyAuthority("USER", "ADMIN")
                    .anyRequest().
                    authenticated()
                    .and()
                    .formLogin()
                    .loginPage("/login")
                    .permitAll()
                    .defaultSuccessUrl("/menu")
                    .and()
                    .logout()
                    .permitAll()
                    .and()
                    .exceptionHandling().accessDeniedPage("/access-denied.html")
                    .and()
                    .exceptionHandling().accessDeniedHandler(accessDeniedHandler());

//             http.csrf()
//                .ignoringAntMatchers("/h2-console/**");
             http.headers()
                .frameOptions()
                .sameOrigin();


    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
     auth.userDetailsService(userService)
     .passwordEncoder(bCryptPasswordEncoder);
    }

    @Bean
    public AccessDeniedHandler accessDeniedHandler(){
        return new CustomAccessDeniedHandler();
    }
}
