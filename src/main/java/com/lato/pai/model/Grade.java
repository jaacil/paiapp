package com.lato.pai.model;


import javax.persistence.*;

@Entity(name = "Grades")
public class Grade {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column (nullable = false, length = 5)
    private double grade;

    @Column (nullable = false, length = 50)
    private String desc;

    @ManyToOne(targetEntity = User.class)
    User user;

    @ManyToOne(targetEntity = Admin.class)
    Admin admin;

    @ManyToOne(targetEntity = Subject.class)
    Subject subject;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    @Override
    public String toString() {
        return "Grade{" +
                "id=" + id +
                ", grade=" + grade +
                ", desc='" + desc + '\'' +
                ", user=" + user.getName() +
                ", admin=" + admin.getName() +
                ", subject=" + subject.getName() +
                '}';
    }
}
