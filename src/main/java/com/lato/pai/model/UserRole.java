package com.lato.pai.model;

public enum UserRole {

    ADMIN, USER
}