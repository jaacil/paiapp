package com.lato.pai.service;

import com.lato.pai.model.UserClass;
import com.lato.pai.repository.UserClassRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserClassService {

    @Autowired
    UserClassRepository userClassRepository;

    public void assignStudentToClass(UserClass userClass) {
        userClassRepository.save(userClass);
    }

    public  List<UserClass> userClass(String name){
        return userClassRepository.findByClassList_Name(name);
    }

    public List<UserClass> findAllByClassID(int id) {
        return userClassRepository.findAllByClassList_Id(id);
    }

    public void deleteUserClass(UserClass userClass) {
        userClassRepository.delete(userClass);
    }

}
