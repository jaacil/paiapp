package com.lato.pai.service;

import com.lato.pai.model.*;
import com.lato.pai.repository.AdminRepository;
import com.lato.pai.repository.ClassListRepository;
import com.lato.pai.repository.UserClassRepository;
import com.lato.pai.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.text.MessageFormat;
import java.util.List;

@Service
@AllArgsConstructor
public class UserService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AdminRepository adminRepository;

    @Autowired
    ClassListRepository classListRepository;

    @Autowired
    UserClassRepository userClassRepository;

    @Autowired
    ConfirmationTokenService confirmationTokenService;

    @Autowired
    EmailService emailService;

    @Autowired
    UserClassService userClassService;

    private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    public void save(User user) {
        userRepository.save(user);
    }

    public User checkIfEmailExist(String email) {
        return userRepository.findByEmail(email);
    }

    public User checkIfLoginExist(String login) {
        return userRepository.findByLogin(login);
    }

    public List<User> findAllStudents(){
        return userRepository.findAll();
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        final User user = userRepository.findByLogin(login);
        final Admin admin = adminRepository.findByLogin(login);

        if(user == null && admin == null) {
            throw new UsernameNotFoundException(MessageFormat.format("User with login {0} cannot be found.", login));
        } else if (user != null) {
            return user;
        } else {
            return admin;
        }
//        if(user != null) {
//            return user;
//        } else {
//            throw new UsernameNotFoundException(MessageFormat.format("User with login {0} cannot be found.", login));
//        }
    }

    public void signUpUser(User user) {


        final String encryptedPassword = bCryptPasswordEncoder.encode(user.getPassword());

        user.setPassword(encryptedPassword);
        userRepository.save(user); //final User createdUser =
        final ConfirmationToken confirmationToken = new ConfirmationToken(user);

        confirmationTokenService.saveConfirmationToken(confirmationToken);
        sendConfirmationMail(user.getEmail(), confirmationToken.getConfirmationToken());

        ClassList classList = classListRepository.findByName(user.getClassNo());
        UserClass userClass = new UserClass();

        userClass.setClassList(classList);
        userClass.setUser(user);

        userClassService.assignStudentToClass(userClass);
    }

    public void signUpAdmin(Admin admin) {

        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        final String encryptedPassword = bCryptPasswordEncoder.encode(admin.getPassword());

        admin.setPassword(encryptedPassword);
        adminRepository.save(admin); //final User createdUser =
    }

    public void confirmUser(ConfirmationToken confirmationToken) {

        final User user = confirmationToken.getUser();

        user.setEnabled(true);

        userRepository.save(user);

        confirmationTokenService.deleteConfirmationToken(confirmationToken.getId());

    }

    public void sendConfirmationMail(String userMail, String token) {
        final SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(userMail);
        mailMessage.setSubject("Account Confirmation");
        mailMessage.setFrom("<MAIL>");
        mailMessage.setText("Your account was created. Please click to activate " + "http://localhost:8080/confirm?token=" + token);

        emailService.sendEmail(mailMessage);
    }

    public void updateStudent(User student, Principal principal) {

        User updatedStudent = userRepository.findByLogin(principal.getName());

        //zmiana danych - loginu, imienia i emaila nie mozna. Haslo w oddzielnej edycji
        updatedStudent.setSurname(student.getSurname());
        updatedStudent.setAddress1(student.getAddress1());
        updatedStudent.setAddress2(student.getAddress2());
        updatedStudent.setEmail(student.getEmail());
        updatedStudent.setPhoneNumber(student.getPhoneNumber());

        userRepository.save(updatedStudent);
    }

    public void updateStudentByAdmin(User student) {

        User updatedStudent = userRepository.findById(student.getId());
        UserClass userClass = userClassRepository.findByClassList_Name(updatedStudent.getClassNo()).get(0);
        ClassList classList = classListRepository.findByName(student.getClassNo());

        userClass.setClassList(classList);

        updatedStudent.setLogin(student.getLogin());
        updatedStudent.setName(student.getName());
        updatedStudent.setSurname(student.getSurname());
        updatedStudent.setAddress1(student.getAddress1());
        updatedStudent.setAddress2(student.getAddress2());
        updatedStudent.setEmail(student.getEmail());
        updatedStudent.setPhoneNumber(student.getPhoneNumber());
        updatedStudent.setClassNo(student.getClassNo());

        userClassRepository.save(userClass);
        userRepository.save(updatedStudent);
    }

    public void changePassword(Principal principal, String oldPasswd, String newPasswd) throws Exception {

        String userName = principal.getName();

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        boolean isAdmin = auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ADMIN"));



        if(isAdmin) {
            Admin admin = adminRepository.findByLogin(userName);
            final String encryptedNewPassword = bCryptPasswordEncoder.encode(newPasswd);
//            System.out.println("haslo stare w bazie " + admin.getPassword());
//            System.out.println("haslo stare w formularzu " + oldPasswd);
            if(bCryptPasswordEncoder.matches(oldPasswd, admin.getPassword())) {
                admin.setPassword(encryptedNewPassword);
                adminRepository.save(admin);
            } else {
                throw new Exception(MessageFormat.format("The old password {0} don't match.", oldPasswd));
            }
        } else {
            User student = userRepository.findByLogin(userName);
            final String encryptedNewPassword = bCryptPasswordEncoder.encode(newPasswd);
            if(bCryptPasswordEncoder.matches(oldPasswd, student.getPassword())) {
                student.setPassword(encryptedNewPassword);
                userRepository.save(student);
            } else {
                throw new Exception(MessageFormat.format("The old password {0} don't match.", oldPasswd));
            }
        }
    }

}
