package com.lato.pai.service;

import com.lato.pai.model.Grade;
import com.lato.pai.model.User;
import com.lato.pai.repository.GradeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GradeService {

    @Autowired
    GradeRepository gradeRepository;

    public List<Grade> findAllUserGrades(User student) {
        return gradeRepository.findAllByUserOrderBySubjectDesc(student);
    }

    public List<Grade> findUserGrades(int id) {
        return gradeRepository.findAllByUserId(id);
    }



}
