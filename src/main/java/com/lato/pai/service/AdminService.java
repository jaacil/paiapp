package com.lato.pai.service;

import com.lato.pai.model.Admin;
import com.lato.pai.model.User;
import com.lato.pai.repository.AdminRepository;
import com.lato.pai.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminService {

    @Autowired
    AdminRepository adminRepository;

    public void save(Admin admin) {
        adminRepository.save(admin);
    }

    public Admin checkIfAdminEmailExist(String email) {
        return adminRepository.findByEmail(email);
    }

    public Admin checkIfAdminLoginExist(String login) {
        return adminRepository.findByLogin(login);
    }
}
