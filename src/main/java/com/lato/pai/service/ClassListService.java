package com.lato.pai.service;

import com.lato.pai.model.ClassList;
import com.lato.pai.repository.ClassListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClassListService {

    @Autowired
    ClassListRepository classListRepository;

    public List<ClassList> findAllClasses() {
        return classListRepository.findAll();
    }

}
